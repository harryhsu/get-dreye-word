<?php
require('simplehtmldom/simple_html_dom.php');

$is_cli     = (! isset($_SERVER['HTTP_USER_AGENT']));
$counter    = 1;
$dict_url   = 'http://yun.dreye.com/dict_new/dict.php?w=';

$gept_list  = 'txt/dreye_gept01.txt';
$gept       = fopen($gept_list, 'a+b');
$gept_content = file_get_contents($gept_list);

$mp3_list   = 'txt/mp3_list.txt';
$mp3_handle = fopen($mp3_list, 'a+b');
$mp3_content = file_get_contents($mp3_list);

$fail_list  = sprintf('fail_%s.txt', date('Ymd_His'));
$wh         = fopen($fail_list, 'wb');

$ignore_list = 'txt/ignore.txt';
$ih = fopen($ignore_list, 'wb');

if ($is_cli && isset($argv[1]) && strpos($argv[1], '.txt') !== false) {
    $words_file = $argv[1];
} elseif (isset($_GET['words']) && strpos($_GET['words'], '.txt') !== false) {
    $words_file = $_GET['words'];
} else {
    $words_file = 'txt/dictionary.txt';
}

$words = fopen($words_file, 'r');

// file_put_contents($gept_list, '');
// file_put_contents($mp3_list, '');

if ($words) {
    while (($line = fgets($words)) !== false) {
        $line          = rtrim($line);
        $filename      = $line . '.mp3';
        $save_filename = 'sound/' . $filename;
        $word          = getElement($dict_url . $line);

        printf("%s\n", (($is_cli) ? $line : $line . "<br>"));

        // 如果已存在清單中，則略過
        // if (strpos($gept_content, $line) !== false && strpos($mp3_content, $line) !== false) {
        if (preg_match_all('/^' . $line . '\[/m', $gept_content)) {
            fwrite($ih, sprintf("%s\n", $line));
            printf("pass\n%s", (($is_cli) ? "====================\n" : '<hr>'));
            continue;
        }

        // 依執行環境不同，顯示不同執行結果
        if ($is_cli) {
            printf("%s\n%s\n", $word['phonetic'], $word['description']);
        } else {
            printf("<p>%s<br>%s</p>", $word['phonetic'], $word['description']);
            printf("<p><audio src='%s' controls></audio>%s</p>", $word['mp3'], $word['mp3']);
        }

        if (! $word['mp3']) {
            // 儲存下載失敗的單字
            fwrite($wh, sprintf("%s\n", $line));
        } else {
            // 儲存發音檔url
            fwrite($mp3_handle, sprintf("%s\n", $word['mp3']));
        }

        // 如發音檔不存在或大小為0，則下載
        if (! file_exists($save_filename) || filesize($save_filename) == 0) {
            printf("%s\n", (($is_cli) ? "download mp3" : "download mp3<br>"));
            $content = file_get_contents($word['mp3']);
            file_put_contents($save_filename, $content);
        }

        if (@filesize($save_filename) === 0) {
            unlink($save_filename);
        }

        printf("filesize: %s\n", @filesize($save_filename));

        // 儲存單字、中文解釋、音標
        fwrite($gept, sprintf("%s[sound:%s];%s;%s\n", $line, $filename, $word['description'], $word['phonetic']));

        echo ($is_cli) ? "====================\n" : '<hr>';

        // usleep(100000);
    }

    fclose($words);
    fclose($wh);
    fclose($gept);
} else {
    echo 'can\'t open words file';
}

function getElement($url) {
    $word    = array();
    $mp3_url = 'http://yun.dreye.com/dict_new/media/';
    $counter = 0;

    do {
        $str_temp = '';

        if ($counter > 10) {
            break;
        } elseif ($counter > 0) {
            $seconds = 100000 * $counter;
            printf("retry: %s, wait: %s seconds\n", $counter, $seconds/1000000);
            usleep($seconds);
        }

        $html = file_get_html($url);

        if (! $html) {
            break;
        } else {
            $word['description'] = $html->find('meta[property=og:description]', 0)->getAttribute('content');
            $word['phonetic']    = $html->find('.phonetic', 0)->innertext;

            // $str_temp    = strstr($html->search_noise('.mp3'), $mp3_url);
            $str_temp = strstr($html->find('script', 4)->innertext, $mp3_url);
            $word['mp3'] = substr($str_temp, 0, strpos($str_temp, ".mp3") + 4);
        }

        $counter++;
    } while (! $word['mp3']);

    return $word;
}
?>
